#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double mysqrt(double x)
{
	if(x < 0)
		return -1;
	double g = x;
	while(fabs(g*g - x) > 0.000001)
	{
		g = (g+x/g)/2;
	}
	return g;
}

