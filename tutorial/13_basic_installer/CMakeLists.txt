cmake_minimum_required(VERSION 2.8)
project(Tutorial)
# The version number
set (Tutorial_VERSION_MAJOR 1)
set (Tutorial_VERSION_MINOR 0)

# does this system provide the log and exp functions ?
include (CheckFunctionExists)
check_function_exists (log HAVE_LOG)
check_function_exists (exp HAVE_EXP)
check_function_exists (sqrt HAVE_SQRT)
check_function_exists (open HAVE_OPEN)

# does this system provide stdio.h and malloc.h head file ?
include (CheckIncludeFiles)
check_include_files (stdio.h HAVE_STDIO_H)
check_include_files (malloc.h HAVE_MALLOC_H)

# does this system provide math pthread library ?
include (CheckLibraryExists)
#check_library_exists (LIBRARY FUNCTION LOCATION VARIABLE)
check_library_exists (m sqrt "" HAVE_MATH_LIBRARY)
check_library_exists (pthread pthread_create "" HAVE_THREAD_LIBRARY)

# does this system provide zlib module ?
find_package (ZLIB REQUIRED)
if (ZLIB_FOUND)
	include_directories(${ZLIB_INCLUDE_DIRS})
	set (EXTRA_LIBS ${EXTRA_LIBS} ${ZLIB_LIBRARIES})
endif (ZLIB_FOUND)

link_libraries(m)

# add the binary tree to the search path for include files
# so that we will find TutorialConfig.h
include_directories("${PROJECT_BINARY_DIR}")

# should we use our own math functions ?
option(USE_MYMATH
	"Use tutorial provided math implementation" ON)

# add the MathFunctions library ?
if (USE_MYMATH)
	include_directories("${PROJECT_SOURCE_DIR}/MathFunctions")
	add_subdirectory(MathFunctions)
	set(EXTRA_LIBS ${EXTRA_LIBS} MathFunctions)
endif (USE_MYMATH)

# configure a header file to pass some of the CMake settings
# to the source code
configure_file (
 "${PROJECT_SOURCE_DIR}/TutorialConfig.h.in"
 "${PROJECT_BINARY_DIR}/TutorialConfig.h"
)

# add source file
aux_source_directory(. SRCS)

# add the executable
add_executable(Tutorial ${SRCS})
target_link_libraries(Tutorial ${EXTRA_LIBS})

# add the install targets
install (TARGETS Tutorial DESTINATION bin)
install (FILES "${PROJECT_BINARY_DIR}/TutorialConfig.h" DESTINATION include)

include (CTest)

# define a macro to simplify adding tests, then use it
macro (do_test arg result)
	add_test(TutorialComp${arg} Tutorial ${arg})
	set_tests_properties(TutorialComp${arg} PROPERTIES PASS_REGULAR_EXPRESSION ${result})
endmacro (do_test)

# does the application run
add_test (TutorialRuns Tutorial 25)
do_test (4 "4 is 2")
do_test (9 "9 is 3")
# does it sqrt of 25
do_test (25 "25 is 5")
# does it handle negative numbers
do_test (-25 "-25 is 0")
# does it handle small numbers
do_test (0.0001 "0.0001 is 0.01")
# does the usage message work ?
add_test (TutorialUsage Tutorial)
set_tests_properties (TutorialUsage PROPERTIES PASS_REGULAR_EXPRESSION "Usage:.*number")


# build a CPack driven installer package
include (InstallRequiredSystemLibraries)
set (CPACK_RESOURCE_FILE_LICENSE
	"${CMAKE_CURRENT_SOURCE_DIR}/License.txt")
set (CPACK_PACKAGE_VERSION_MAJOR "${Tutorial_VERSION_MAJOR}")
set (CPACK_PACKAGE_VERSION_MINOR "${Tutorial_VERSION_MINOR}")
include (CPack)



