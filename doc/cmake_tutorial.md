本文翻译自`cmake tutorial`：https://cmake.org/cmake-tutorial/

本文不介绍cmake命令使用方法，也不讲`CMakeLists.txt`的语法，有需要的读者可以看我另外相关的文章即可（此为实战篇）。废话少说，直接上主菜（复杂的解释也略过了）。

有关常见的问题，可上官网Wiki查看：https://gitlab.kitware.com/cmake/community/wikis/FAQ。

# 一、基本工程

最简单的工程是编译一个源文件，并生成一个可执行文件。`CMakeLists.txt`仅仅只需要3行：

```
cmake_minimum_required(VERSION 2.8)
project(Tutorial)
add_executable(Tutorial tutorial.cxx)
```

对应的`tutorial.cxx`源码用于计算输入数据的平方根：

```
// A simple program that computes the suqare root of a number
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		fprintf(stdout, "Usage: %s number\n", argv[0]);
		return 1;
	}

	double inputValue = atof(argv[1]);
	double outputValue = sqrt(inputValue);
	fprintf(stdout, "The square root of %g is %g\n", inputValue, outputValue);

	return 0;
}
```

将`tutorial.cxx`与`CMakeLists.txt`放在同一目录，创建build目录，并编译、运行，结果如下：

```
$ mkdir build
$ ls
build  CMakeLists.txt  tutorial.cxx
$ cd build/
$ cmake ..
-- The C compiler identification is GNU 4.8.5
-- The CXX compiler identification is GNU 4.8.5
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /work/study/Studied_Module/Module/cmake/basic_point/build
$ make
Scanning dependencies of target Tutorial
[ 50%] Building CXX object CMakeFiles/Tutorial.dir/tutorial.cxx.o
[100%] Linking CXX executable Tutorial
[100%] Built target Tutorial
$ ./Tutorial 
Usage: ./Tutorial number
$ ./Tutorial 9
The square root of 9 is 3
```

# 二、添加配置头文件

有时候我们需要为我们的代码添加版本信息，或者其他相关的配置信息。我们当然可以通过直接添加配置的头文件来实现，不过通过`CMakeLists.txt`来实现会更加的灵活。下面是基于前面例子实现添加版本信息的`CMakeLists.txt`：

```
cmake_minimum_required(VERSION 2.8)
project(Tutorial)
# The version number
set (Tutorial_VERSION_MAJOR 1)
set (Tutorial_VERSION_MINOR 0)

# configure a header file to pass some of the CMake settings
# to the source code
configure_file (
 "${PROJECT_SOURCE_DIR}/TutorialConfig.h.in"
 "${PROJECT_BINARY_DIR}/TutorialConfig.h"
)

# add the binary tree to the search path for include files
# so that we will find TutorialConfig.h
include_directories("${PROJECT_BINARY_DIR}")

# add the executable
add_executable(Tutorial tutorial.cxx)
```

上面指定`TutorialConfig.h`由`TutorialConfig.h.in`文件生成。`TutorialConfig.h.in`的内容如下：

```
// the configured options and settings for Tutorial
#define Tutorial_VERSION_MAJOR @Tutorial_VERSION_MAJOR@
#define Tutorial_VERSION_MINOR @Tutorial_VERSION_MINOR@
```

当`CMake`配置这个头文件时，`@Tutorial_VERSION_MAJOR@`和`@Tutorial_VERSION_MINOR@`的值将被`CMakeLists.txt`文件中的值替换（注意两边的名字要一致）。

接下来我们修改`tutorial.cxx`源码，看是如何使用`CMakeLists.txt`定义的版本的：

```
// A simple program that computes the suqare root of a number
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "TutorialConfig.h"

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		fprintf(stdout, "%s Version %d.%d\n", argv[0],
			Tutorial_VERSION_MAJOR, Tutorial_VERSION_MINOR);
		fprintf(stdout, "Usage: %s number\n", argv[0]);
		return 1;
	}

	double inputValue = atof(argv[1]);
	double outputValue = sqrt(inputValue);
	fprintf(stdout, "The square root of %g is %g\n", inputValue, outputValue);

	return 0;
}
```

建立`build`目录，再次编译结果如下：

```
$ mkdir build
$ ls
build  CMakeLists.txt  TutorialConfig.h.in  tutorial.cxx
$ cd build/
$ cmake ..
-- The C compiler identification is GNU 4.8.5
-- The CXX compiler identification is GNU 4.8.5
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /work/study/Studied_Module/Module/cmake/basic_version/build
$ make
Scanning dependencies of target Tutorial
[ 50%] Building CXX object CMakeFiles/Tutorial.dir/tutorial.cxx.o
[100%] Linking CXX executable Tutorial
[100%] Built target Tutorial
$ ./Tutorial 
./Tutorial Version 1.0
Usage: ./Tutorial number
$ ./Tutorial 16
The square root of 16 is 4

```

总结：添加配置头文件的流程如下：
1. 在`CMakeLists.txt`中通过set命令设置自己命名的变量；
2. 添加配置文件说明：

```
configure_file (
 "${PROJECT_SOURCE_DIR}/TutorialConfig.h.in"
 "${PROJECT_BINARY_DIR}/TutorialConfig.h"
)
```

3. 创建配置文件`*.h.in`，并使用`@variable@`来访问第一步在`CMakeLists.txt`通过set定义的变量，使用`C/C++`的标准#define来定义；
4. 在`C/C++`源码中通过include包含编译生成的配置头文件；
5. 在`C/C++`源码中直接使用#define宏定义的名称。

# 三、添加一个库

继续基于前面的实例，我们将计算平方根的方法提取出来，做成一个`mysqrt.cxx`源文件库放在`MathFunctions`目录，我们提供的算法名称为`mysqrt()`。源码如下：

```
$ cd MathFunctions/
$ cat mysqrt.h 
#ifndef MYSQRT_H__
#define MYSQRT_H__

double mysqrt(double x);

#endif

$ cat mysqrt.cxx 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double mysqrt(double x)
{
	double g = x;
	while(fabs(g*g - x) > 0.000001)
	{
		g = (g+x/g)/2;
	}
	return g;
}
```

在MathFunctions目录中添加子`CMakeLists.txt`，编译生成库文件：

```
$ cat CMakeLists.txt 
cmake_minimum_required(VERSION 2.8)
project(MathFunctions)
add_library(MathFunctions SHARED mysqrt.cxx)
```

接下来修改顶层`CMakeLists.txt`，添加子目录`MathFunctions`的编译支持，添加头文件依赖路径，并添加依赖的库说明：

```
include_directories("${PROJECT_SOURCE_DIR}/MathFunctions")
add_library(MathFuncions)

# add the executable
add_executable(Tutorial tutorial.cxx)
add_link_libraries(Tutorial MathFunctions)
```

最后，修改`tutorial.cxx`源码，添加代码调用我们自己的`mysqrt()`函数（这里同时调用，还可以验证我们的算法是否正确）：

```
    double inputValue = atof(argv[1]);
	double outputValue = sqrt(inputValue);
	fprintf(stdout, "The square root of %g is %g\n", inputValue, outputValue);
	outputValue = mysqrt(inputValue);
	fprintf(stdout, "The square root of %g is %g\n", inputValue, outputValue);
```

# 四、添加编译选项

继续接着前面的实例，我们添加了自己的`mysqrt()`函数，但是我们并不一定都是使用自己的`mysqrt()`函数，当系统提供了`sqrt()`时，我们肯定希望调用系统标准的函数。也就是说，我们需要根据系统情况，动态选择调用方法。

我们当然可以通过在头文件中添加编译选项来实现，不过通过`CMakeLists.txt`来实现将更加灵活有趣。

首先，我们定义一个编译宏USE_MYMATH（在顶层`CMakeLists.txt`）：

```
# should we use our own math functions ?
option(USE_MYMATH
    "Use tutorial provided math implementation" ON)
```

这将告诉CMake默认打开`USE_MYMATH`，当然也可以通过`ccmake`或者编译选项来重新配置。接下来我们在顶层`CMakeLists.txt`同时添加依赖库的说明：

```
# add the MathFunctions library ?
#
if (USE_MYMATH)
    include_directories("${PROJECT_SOURCE_DIR}/MathFunctions")
    add_subdirectory(MathFunctions)
    set(EXTRA_LIBS ${EXTRA_LIBS} MathFunctions)
#endif(USE_MYMATH)

# add the executable
add_executable(Tutorial tutorial.cxx)
target_link_libraries(Tutorial ${EXTRA_LIBS})
```

注意：**上面添加的`USE_MYMATH`选项的`option`命令，必须在`configure_file`命令之前，否则其默认ON的配置将会无效**。

接下来，修改`tutorial.cxx`源代码，添加条件编译代码：

```
// A simple program that computes the suqare root of a number
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "TutorialConfig.h"

#ifdef USE_MYMATH
#include "mysqrt.h"
#endif

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		fprintf(stdout, "%s Version %d.%d\n", argv[0],
			Tutorial_VERSION_MAJOR, Tutorial_VERSION_MINOR);
		fprintf(stdout, "Usage: %s number\n", argv[0]);
		return 1;
	}

	double inputValue = atof(argv[1]);

	#ifdef USE_MYMATH
	double outputValue = mysqrt(inputValue);
	fprintf(stdout, "The square root of %g is %g(use tutorial mysqrt())\n", inputValue, outputValue);
	#else
	double outputValue = sqrt(inputValue);
	fprintf(stdout, "The square root of %g is %g(use system sqrt())\n", inputValue, outputValue);
	#endif

	return 0;
}
```

最后，在`TutorialConfig.h.in`中添加支持，以便在CMake命令行传入不同的编译选项时生成不同的`TutorialConfig.h`文件：

```
#cmakedefine USE_MYMATH
```

创建build目录，并编译验证结果如下：

使用`mysqrt()`：

```
$ mkdir build
$ ls
build  CMakeLists.txt  MathFunctions  TutorialConfig.h.in  tutorial.cxx
$ cd build/
[study@konishi build]$ cmake -DUSE_MYMATH=True ..
-- The C compiler identification is GNU 4.8.5
-- The CXX compiler identification is GNU 4.8.5
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /work/study/Studied_Module/Module/cmake/04_basic_options/build
$ make
Scanning dependencies of target MathFunctions
[ 25%] Building CXX object MathFunctions/CMakeFiles/MathFunctions.dir/mysqrt.cxx.o
[ 50%] Linking CXX shared library libMathFunctions.so
[ 50%] Built target MathFunctions
Scanning dependencies of target Tutorial
[ 75%] Building CXX object CMakeFiles/Tutorial.dir/tutorial.cxx.o
[100%] Linking CXX executable Tutorial
[100%] Built target Tutorial
$ ./Tutorial 
./Tutorial Version 1.0
Usage: ./Tutorial number
$ ./Tutorial  12
The square root of 12 is 3.4641(use tutorial mysqrt())
```

使用系统的sqrt()：

```
$ mkdir build
$ ls
build  CMakeLists.txt  MathFunctions  TutorialConfig.h.in  tutorial.cxx
$ cd build/
$ cmake -DUSE_MYMATH=False ..
-- The C compiler identification is GNU 4.8.5
-- The CXX compiler identification is GNU 4.8.5
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /work/study/Studied_Module/Module/cmake/04_basic_options/build
$ make
Scanning dependencies of target Tutorial
[ 50%] Building CXX object CMakeFiles/Tutorial.dir/tutorial.cxx.o
[100%] Linking CXX executable Tutorial
[100%] Built target Tutorial
[study@konishi build]$ ./Tutorial 
./Tutorial Version 1.0
Usage: ./Tutorial number
$ ./Tutorial 13
The square root of 13 is 3.60555(use system sqrt())
```

总结：添加配置选项的流程如下：
1. 在`CMakeLists.txt`中通过option命令设置自己定义的编译选项；
2. 添加配置文件说明：

```
configure_file (
 "${PROJECT_SOURCE_DIR}/TutorialConfig.h.in"
 "${PROJECT_BINARY_DIR}/TutorialConfig.h"
)
```

3. 若有需要，使用`if()`条件，添加使能对应配置选项所要依赖的目录、库、编译子系统等；
4. 创建配置文件`*.h.in`，使用`#cmakedefine`定义第一步定义的编译选项；

```
#cmakedefine USE_MYMATH
```
5. 在`C/C++`源码中通过`include`包含编译生成的配置头文件；
6. 在`C/C++`源码中直接使用`#ifdef`等宏编译命令，后面接第一步定义的编译选项名称即可使用。

# 五、为库添加版本

基于前的例程，我们为我们的`MathFunctions`库添加对应的版本支持。其实很简单，只需要在子编译模块`MathFunctions`中的`CMakeLists.txt`中添加如下语句即可：

```
set_target_properties(MathFunctions PROPERTIES VERSION 0.1 SOVERSION 1)
```

这就将我们的库`MathFunctions`添加了版本V0.1，API接口版本为1。详见实例`05_basic_lib_version`。

# 六、添加多源代码支持

基于前面的例程，我们发现我们都是将源代码（*.cxx）直接在`CMakeLists.txt`中明确写明的。但当我们的源代码数量很庞大的情况下，做任何添加和修改都将非常的繁琐。为此，我们可以使用CMake的`aux_source_directory`命令直接指定搜索路径。

比如对应前面的实例，只需要将顶层`CMakeLists.txt`中的：

```
add_executable(Tutorial tutorial.cxx)
```

修改为：

```
aux_source_directory(. SRCS)
add_executable(Tutorial ${SRCS})
```

将`MathFunctions`子目录的`CMakeLists.txt`中的：

```
add_library(MathFunctions SHARED mysqrt.cxx)
```

修改为：

```
aux_source_directory(. MY_SRCS)
add_library(MathFunctions SHARED ${MY_SRCS})
```

即可。详细请见`06_basic_mult_source`。

# 七、添加安装支持

接下来，我们为前面完成的工程添加安装支持。首先在`MathFunctions`目录的`CMakeLists.txt`添加如下两行：

```
install (TARGETS MathFunctions DESTINATION lib)
install (FILES mysqrt.h DESTINATION include)
```

第一行将会把我们生成的目标`MathFunctions`库安装到指定目录（`--prefix`）的lib目录下；第二行将会把头文件`mysqrt.h`安装到指定目录的include目录下。

同时，修改顶层目录的`CMakeLists.txt`，添加如下两行：

```
# add the install targets
install (TARGETS Tutorial DESTINATION bin)
install (FILES "${PROJECT_BINARY_DIR}/TutorialConfig.h" DESTINATION include)
```

第一行会将我们生成的可执行文件`Tutorial`安装到bin目录；第二行则将CMake生成的配置头文件`TutorialConfig.h`安装到include目录。

创建build目录，cmake并编译安装，结果如下：

```
$ mkdir build linux
$ ls
build  CMakeLists.txt  linux  MathFunctions  TutorialConfig.h.in  tutorial.cxx
$ cd build/
$  cmake -DUSE_MYMATH=True -DCMAKE_INSTALL_PREFIX=$(pwd)/../linux ../
-- The C compiler identification is GNU 4.8.5
-- The CXX compiler identification is GNU 4.8.5
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /work/study/Studied_Module/Module/cmake/07_basic_install/build
$ make
Scanning dependencies of target MathFunctions
[ 25%] Building CXX object MathFunctions/CMakeFiles/MathFunctions.dir/mysqrt.cxx.o
[ 50%] Linking CXX shared library libMathFunctions.so
[ 50%] Built target MathFunctions
Scanning dependencies of target Tutorial
[ 75%] Building CXX object CMakeFiles/Tutorial.dir/tutorial.cxx.o
[100%] Linking CXX executable Tutorial
[100%] Built target Tutorial
$ make install
[ 50%] Built target MathFunctions
[100%] Built target Tutorial
Install the project...
-- Install configuration: ""
-- Installing: /work/study/Studied_Module/Module/cmake/07_basic_install/linux/bin/Tutorial
-- Set runtime path of "/work/study/Studied_Module/Module/cmake/07_basic_install/linux/bin/Tutorial" to ""
-- Installing: /work/study/Studied_Module/Module/cmake/07_basic_install/linux/include/TutorialConfig.h
-- Installing: /work/study/Studied_Module/Module/cmake/07_basic_install/linux/lib/libMathFunctions.so.0.1
-- Installing: /work/study/Studied_Module/Module/cmake/07_basic_install/linux/lib/libMathFunctions.so.1
-- Installing: /work/study/Studied_Module/Module/cmake/07_basic_install/linux/lib/libMathFunctions.so
-- Installing: /work/study/Studied_Module/Module/cmake/07_basic_install/linux/include/mysqrt.h
$ cd ../linux/
$ ls
bin  include  lib
$ ls bin/
Tutorial
$ ls include/
mysqrt.h  TutorialConfig.h
$ ls lib/
libMathFunctions.so  libMathFunctions.so.0.1  libMathFunctions.so.1
```

可以看到，`Tutorial`可执行程序安装到了`linux/bin`目录，`mysqrt.h`和`TutorialConfig.h`安装到了`linux/include`目录，`MathFunctions`库则安装到了`linux/lib`目录下面（通过`CMAKE_INSTALL_PREFIX`可以指定安装路径）。

通过前面这7步的学习，已经基本满足项目使用的需求了。

# 八、添加测试

为了验证我们的应用和库是否正常运转，我们可以添加测试的支持。对应在顶层的`CMakeLists.txt`文件中，添加如下代码：

```
include (CTest)

# does the application run
add_test (TutorialRuns Tutorial 25)
# does it sqrt of 25
add_test (TutorialComp25 Tutorial 25)
set_tests_properties (TutorialComp25 PROPERTIES PASS_REGULAR_EXPRESSION "25 is 5")
# does it handle negative numbers
add_test (TutorialNegative Tutorial -25)
set_tests_properties (TutorialNegative PROPERTIES PASS_REGULAR_EXPRESSION "-25 is 0")
# does it handle small numbers
add_test (TutorialSmall Tutorial 0.0001)
set_tests_properties (TutorialSmall PROPERTIES PASS_REGULAR_EXPRESSION "0.0001 is 0.01")
# does the usage message work ?
add_test (TutorialUsage Tutorial)
set_tests_properties (TutorialUsage PROPERTIES PASS_REGULAR_EXPRESSION "Usage:.*number")
```

当我们在build执行cmake和make之后，就可以使用`make test`来进行测试验证了：

```
$ make test
Running tests...
Test project /work/study/Studied_Module/Module/cmake/08_basic_testing/build
    Start 1: TutorialRuns
1/5 Test #1: TutorialRuns .....................   Passed    0.00 sec
    Start 2: TutorialComp25
2/5 Test #2: TutorialComp25 ...................   Passed    0.00 sec
    Start 3: TutorialNegative
3/5 Test #3: TutorialNegative .................***Failed  Required regular expression not found.Regex=[-25 is 0
]  0.00 sec
    Start 4: TutorialSmall
4/5 Test #4: TutorialSmall ....................   Passed    0.00 sec
    Start 5: TutorialUsage
5/5 Test #5: TutorialUsage ....................   Passed    0.00 sec

80% tests passed, 1 tests failed out of 5

Total Test time (real) =   0.01 sec

The following tests FAILED:
	  3 - TutorialNegative (Failed)
Errors while running CTest
make: *** [test] Error 8
```

测试结果为5个通过4个，其中第3个未通过。

当我们的测试非常多时，可以使用CMake语法的macro定义函数来简化编写：

```
include (CTest)

# define a macro to simplify adding tests, then use it
macro (do_test arg result)
	add_test(TutorialComp${arg} Tutorial ${arg})
	set_tests_properties(TutorialComp${arg} PROPERTIES PASS_REGULAR_EXPRESSION ${result})
endmacro (do_test)

# does the application run
add_test (TutorialRuns Tutorial 25)
# does it sqrt of 25
do_test (25 "25 is 5")
# does it handle negative numbers
do_test (-25 "-25 is 0")
# does it handle small numbers
do_test (0.0001 "0.0001 is 0.01")
# does the usage message work ?
add_test (TutorialUsage Tutorial)
set_tests_properties (TutorialUsage PROPERTIES PASS_REGULAR_EXPRESSION "Usage:.*number")
```

# 九、添加系统模块自检

当我们编写的代码，依赖于系统平台的一些函数，则需要在编译之前对系统依赖进行自检。比如假如我们使用了`log()`和`exp()`函数，我们为其添加检测方法如下：

```
# does this system provide the log and exp functions ?
include (CheckFunctionExists)
check_function_exists (log HAVE_LOG)
check_function_exists (exp HAVE_EXP)
```

对应在TutorialConfig.h.in文件中添加对应配置：
```
// does the platform provide exp and log functions ?
#cmakedefine HAVE_LOG
#cmakedefine HAVE_EXP
```

在`configure_file`命令生成`TutorialConfig.h`文件之前，进行依赖检查是非常重要的。比如上面的log和exp，当系统不提供支持时，我们可以在自己的源代码中，使用替代方案实现：

```
// if we have both log and exp then use them
#if defined (HAVE_LOG) && defined (HAVE_EXP)
  result = exp(log(x)*0.5);
#else // otherwise use an iterative approach
  . . .
```

接下来，创建build目录，然后运行cmake命令，看生成的`TutorialConfig.h`文件：

```
$ cmake -DUSE_MYMATH=False ..
-- The C compiler identification is GNU 4.8.5
-- The CXX compiler identification is GNU 4.8.5
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Looking for log
-- Looking for log - not found
-- Looking for exp
-- Looking for exp - not found
-- Looking for sqrt
-- Looking for sqrt - not found
-- Configuring done
-- Generating done
-- Build files have been written to: /work/study/Studied_Module/Module/cmake/09_basic_introspection/build
$ cat TutorialConfig.h 
// the configured options and settings for Tutorial
#define Tutorial_VERSION_MAJOR 1
#define Tutorial_VERSION_MINOR 0

/* #undef USE_MYMATH */

// does the platform provide exp and log functions ?
/* #undef HAVE_LOG */
/* #undef HAVE_EXP */
/* #undef HAVE_SQRT */
```

# 十、添加系统头文件和库自检

使用第九章同样的风格方法，还可以对系统的头文件和依赖库进行检测。头文件检测：

```
# 在CMakeLists.txt中添加
# does this system provide stdio.h and malloc.h head file ?
include (CheckIncludeFiles)
check_include_files (stdio.h HAVE_STDIO_H)
check_include_files (malloc.h HAVE_MALLOC_H)

# 在TutorialConfig.h.in中添加
// does this system provide stdio.h and malloc.h head file ?
#cmakedefine HAVE_STDIO_H
#cmakedefine HAVE_MALLOC_H
```

依赖库的检测方法为：

```
# 在CMakeLists.txt中添加
# does this system provide math and pthread library ?
include (CheckLibraryExists)
#check_library_exists (LIBRARY FUNCTION LOCATION VARIABLE)
check_library_exists (m sqrt "" HAVE_MATH_LIBRARY)
check_library_exists (pthread pthread_create "" HAVE_THREAD_LIBRARY)

# 在TutorialConfig.h.in中添加
// does this system provide math and thread library ?
#cmakedefine HAVE_MATH_LIBRARY
#cmakedefine HAVE_THREAD_LIBRARY
```

与此类似的检测还有：`CheckSymbolExists`、`CheckTypeSize`、`CheckPrototypeExists`、`CheckCXXSourceCompiles`和`CheckCSourceCompiles`等。

# 十一、检测组件包（模块）

如前面的实例，可检测依赖的头文件和库是否存在。但有些库事先不知道它的头文件和链接库的位置。CMake使用`find_package`命令来解决这个问题。

## 11.1 使用常见库（CMake自带）

为了能支持各种常见的库和包，CMake自带了很多的模块，可以通过命令：

```
cmake --help-module-list
```

得到当前CMake支持的模块列表，或者直接查看模块路径。

我们以`bzip2`库为例，CMake中有一个`FindBZip2.cmake`模块。我们只需要使用`find_package(BZip2)`调用这个模块，cmake会自动给一些变量赋值，然后就可以在`CMakeLists.txt`中使用它们了。变量的列表可以查看cmake的模块文件（如前面的`FindBZip2.cmake`），或者使用命令来查看：

```
$ cmake --help-module FindBZip2
cmake version 2.8.12.2
  FindBZip2
       Try to find BZip2

       Once done this will define

         BZIP2_FOUND - system has BZip2
         BZIP2_INCLUDE_DIR - the BZip2 include directory
         BZIP2_LIBRARIES - Link these to use BZip2
         BZIP2_NEED_PREFIX - this is set if the functions are prefixed with BZ2_
         BZIP2_VERSION_STRING - the version of BZip2 found (since CMake 2.8.8)



       Defined in: /usr/share/cmake/Modules/FindBZip2.cmake
```

如上也说明了系统的cmake相关模块存放在`/usr/share/cmake/Modules/`目录下。

继续以bzip2为例，假如我们的程序使用了bzip2，编译器要知道`bzlib.h`的位置，链接器要知道bzip2库（动态链接的话，Unix上是`libbz2.so`类似文件，Windows上是`libbz2.dll`），则可如下编写`CMakeLists.txt`：

```
cmake_minimum_required(VERSION 2.8)
project(helloworld)
add_executable(helloworld hello.c)
find_package (BZip2)
if (BZIP2_FOUND)
    include_directories(${BZIP_INCLUDE_DIRS})
    target_link_library (helloworld ${BZIP2_LIBRARIES)
endif (BZIP2_FOUND)
```

如上`find_package()`命令，就是在系统cmake的Modules中寻找指定的组件包。上面的BZip2是可选的，若添加REQUIRE选项，则指定的包是必选依赖：

```
find_package (BZip2 REQUIRE)
```

## 11.2 使用外部库（CMake不自带）

假设我们要使用`libXML++`库，而系统的CMake还没有一个`libXML++`的查找模块。但是可以在网上搜索到一个`FindLibXML++.cmake`，那么在`CMakeLists.txt`中写到：

```
find_package(libXML++ REQUIRED)
if (libXML++_FOUND)
    include_directories(${LibXML++_INCLUDE_DIRS})
    set (LIBS ${LIBS} ${LibXML++_LIBRARIES})
endif (libXML++_FOUND)
```

如果包是可选的，可以忽略`REQUIRED`关键字。当检测完所有的库后，对于链接目标可以使用：

```
target_link_libraries(exampleProgram ${LIBS})
```

为了正常工作，需要把`FindLibXML++.cmake`文件放到CMake的模块路径。对于项目而言，也可在项目目录下创建`cmake/Modules`文件夹，然后在`CMakeLists.txt`中指定路径：

```
set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_SOURCE_DIR}/cmake/Modules/)
```

## 11.3 使用实例

对于前面的实例，我们添加ZLIB的支持，首先在命令行下查询ZLIB库模块：

```
$ cmake --help-module FindZLIB
FindZLIB
--------

Find the native ZLIB includes and library.

IMPORTED Targets
^^^^^^^^^^^^^^^^

This module defines ``IMPORTED`` target ``ZLIB::ZLIB``, if
ZLIB has been found.

Result Variables
^^^^^^^^^^^^^^^^

This module defines the following variables:

::

 ZLIB_INCLUDE_DIRS   - where to find zlib.h, etc.
 ZLIB_LIBRARIES      - List of libraries when using zlib.
 ZLIB_FOUND          - True if zlib found.

::

 ZLIB_VERSION_STRING - The version of zlib found (x.y.z)
 ZLIB_VERSION_MAJOR  - The major version of zlib
 ZLIB_VERSION_MINOR  - The minor version of zlib
 ZLIB_VERSION_PATCH  - The patch version of zlib
 ZLIB_VERSION_TWEAK  - The tweak version of zlib

Backward Compatibility
^^^^^^^^^^^^^^^^^^^^^^

The following variable are provided for backward compatibility

::

 ZLIB_MAJOR_VERSION  - The major version of zlib
 ZLIB_MINOR_VERSION  - The minor version of zlib
 ZLIB_PATCH_VERSION  - The patch version of zlib

Hints
^^^^^

A user may set ``ZLIB_ROOT`` to a zlib installation root to tell this
module where to look.
```

可以知道ZLIB库定义了`ZLIB_FOUND`、`ZLIB_LIBRARIES`、`ZLIB_INCLUDE_DIRS`变量。我们修改前面实例顶层的`CMakeLists.txt`，添加如下支持：

```
# does this system provide zlib module ?
find_package (ZLIB REQUIRED)
if (ZLIB_FOUND)
	include_directories(${ZLIB_INCLUDE_DIRS})
	set (EXTRA_LIBS ${EXTRA_LIBS} ${ZLIB_LIBRARIES})
endif (ZLIB_FOUND)
```

然后创建build目录，运行"`cmake ..`"命令结果如下：

```
$ cmake ../
-- The C compiler identification is GNU 4.8.5
-- The CXX compiler identification is GNU 4.8.5
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Looking for log
-- Looking for log - not found
-- Looking for exp
-- Looking for exp - not found
-- Looking for sqrt
-- Looking for sqrt - not found
-- Looking for open
-- Looking for open - found
-- Looking for include file stdio.h
-- Looking for include file stdio.h - found
-- Looking for include file malloc.h
-- Looking for include file malloc.h - found
-- Looking for sqrt in m
-- Looking for sqrt in m - found
-- Looking for pthread_create in pthread
-- Looking for pthread_create in pthread - found
-- Found ZLIB: /usr/lib64/libz.so (found version "1.2.7") 
-- Configuring done
-- Generating done
-- Build files have been written to: /work/study/Studied_Module/Module/cmake/11_basic_find_package/build
```

注意其中的：`Found ZLIB: /usr/lib64/libz.so(found version "1.2.7")`。说明找到了ZLIB库，当前版本为1.2.7。若系统没有，可以通过yum install zlib进行安装。

# 十二、使用间接生成的文件

有时候，我们的项目需要依赖间接生成的文件。例如前面的实例，先建立一个保存事先计算好的平方根的表做为编译的一部分，之后这个表编译到我们的应用中。

要完成这一功能，需要程序生成这个表。我们在前面的实例`MathFunctions`子目录下新建`MakeTable.cxx`来完成生成表的工作：

```
// A simple program that builds a sqrt table
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[])
{
	int i;
	double result;

	// make sure we have enough arguments
	if(argc < 2)
		return -1;

	FILE *fout = fopen(argv[1], "w");
	if(NULL == fout)
		return -1;

	fprintf(fout, "double sqrtTable[] = {\n");
	for(i = 0; i < 10; ++i)
	{
		result = sqrt(static_cast<double>(i));
		fprintf(fout, "%g, \n", result);
	}

	// close the table with a zero
	fprintf(fout, "0};\n");
	fclose(fout);
	return 0;
}
```

注意，这个表是用C++代码生成的，并且输出的文件名是通过参数传递进去的。接下来在`MathFunctions`子目录的`CMakeLists.txt`中添加合适的命令来编译`MakeTable`可执行文件，然后在编译过程中运行这个可执行文件生成我们依赖的表：

```
# first we add the executable that generates the table
add_executable(MakeTable MakeTable.cxx)

# add the command to generate the source code
add_custom_command(
	OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/Table.h
	COMMAND echo "Generate Table.h file"
	COMMAND MakeTable ${CMAKE_CURRENT_BINARY_DIR}/Table.h
	DEPENDS MakeTable
)

# add the binary tree directory to the search path for include files
include_directories(${CMAKE_CURRENT_BINARY_DIR})

aux_source_directory(. MY_SRCS)
add_library(MathFunctions ${MY_SRCS} ${CMAKE_CURRENT_BINARY_DIR}/Table.h)
```

首先，编译`MakeTable`可执行文件与之前介绍一样，然后我们通过`add_custom_command`命令添加一个通过运行`MakeTable`生成`Table.h`的自定义命令。然后，我们让cmake知道`mysqrt.cxx`依赖于生成的`Table.h`（这点很重要，不然自定义的命令不会运行）。最后，我们在`mysqrt.cxx`中用include添加对`Table.h`的真实依赖（通过`include_directories(${CMAKE_CURRENT_BINARY_DIR})`解决了头文件依赖路劲问题）即可。

在顶层目录创建build，然后cmake并编译：

```
$ mkdir build
$ cd build/
$ cmake ../
-- The C compiler identification is GNU 4.8.5
-- The CXX compiler identification is GNU 4.8.5
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Looking for log
-- Looking for log - not found
-- Looking for exp
-- Looking for exp - not found
-- Looking for sqrt
-- Looking for sqrt - not found
-- Looking for open
-- Looking for open - found
-- Looking for include file stdio.h
-- Looking for include file stdio.h - found
-- Looking for include file malloc.h
-- Looking for include file malloc.h - found
-- Looking for sqrt in m
-- Looking for sqrt in m - found
-- Looking for pthread_create in pthread
-- Looking for pthread_create in pthread - found
-- Found ZLIB: /usr/lib64/libz.so (found version "1.2.7") 
-- Configuring done
-- Generating done
-- Build files have been written to: /work/study/Studied_Module/Module/cmake/12_basic_generator/build
[study@konishi build]$ make
Scanning dependencies of target MakeTable
[ 12%] Building CXX object MathFunctions/CMakeFiles/MakeTable.dir/MakeTable.cxx.o
[ 25%] Linking CXX executable MakeTable
[ 25%] Built target MakeTable
[ 37%] Generating Table.h
Generate Table.h file
Scanning dependencies of target MathFunctions
[ 50%] Building CXX object MathFunctions/CMakeFiles/MathFunctions.dir/MakeTable.cxx.o
[ 62%] Building CXX object MathFunctions/CMakeFiles/MathFunctions.dir/mysqrt.cxx.o
[ 75%] Linking CXX static library libMathFunctions.a
[ 75%] Built target MathFunctions
Scanning dependencies of target Tutorial
[ 87%] Building CXX object CMakeFiles/Tutorial.dir/tutorial.cxx.o
[100%] Linking CXX executable Tutorial
[100%] Built target Tutorial
```

注意make命令的执行步骤：
1. 先编译MakeTable可执行文件；
2. 运行MakeTable生成`Table.h`文件；
3. 编译生成`MathFunctions`库文件；
4. 编译生成最终的Tutorial应用程序。

与此类似的命令还有：`add_custom_target()`。

# 十三、生成安装文件

接下来，设想我们发布我们的项目以供他人使用。我们想在很多平台上发布编译结果和代码。

这个过程和之前的安装（`install`）与测试不同，这个例子我们会编译出类似于`cygwin`、`debian`、`rpm`等支持安装和包管理的安装包。

要完成这一功能，我们要使用CPack来生成对应平台的安装包。在代码上，我们需要在顶层的`CMakeLists.txt`中添加如下几行：

```
# build a CPack driven installer package
include (InstallRequiredSystemLibraries)
set (CPACK_RESOURCE_FILE_LICENSE
	"${CMAKE_CURRENT_SOURCE_DIR}/License.txt")
set (CPACK_PACKAGE_VERSION_MAJOR "${Tutorial_VERSION_MAJOR}")
set (CPACK_PACKAGE_VERSION_MINOR "${Tutorial_VERSION_MINOR}")
include (CPack)
```

这就是我们所有的代码了！我们首先`include`了`InstallRequiredSystemLibraries`。这一模块会添加任何这个项目在这个平台所需要的所有运行支持库。接下来我们设置了一些CPack变量，比如`License.txt`和版本号等。最后，我们添加CPack模块，这个模块会使用这些变量和这个系统的其他属性来生成安装包。

接下来，就是按照通常的方式编译项目，之后运行CPack。要编译一个安装包，需要输入如下命令：

```
cpack --config CPackConfig.cmake
```

要生成一个代码安装包，输入如下命令：

```
cpack --config CPackSourceConfig.cmake
```

对于本实例，在顶层目录创建build，然后cmake：

```
$ ls
CMakeLists.txt  License.txt  MathFunctions  TutorialConfig.h.in  tutorial.cxx
$ mkdir build
$ cd build/
$ cmake -DUSE_MYMATH=True ..
-- The C compiler identification is GNU 4.8.5
-- The CXX compiler identification is GNU 4.8.5
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Looking for log
-- Looking for log - not found
-- Looking for exp
-- Looking for exp - not found
-- Looking for sqrt
-- Looking for sqrt - not found
-- Looking for open
-- Looking for open - found
-- Looking for include file stdio.h
-- Looking for include file stdio.h - found
-- Looking for include file malloc.h
-- Looking for include file malloc.h - found
-- Looking for sqrt in m
-- Looking for sqrt in m - found
-- Looking for pthread_create in pthread
-- Looking for pthread_create in pthread - found
-- Found ZLIB: /usr/lib64/libz.so (found version "1.2.7") 
-- Configuring done
-- Generating done
-- Build files have been written to: /work/study/Studied_Module/Module/cmake/13_basic_installer/build
$ make
Scanning dependencies of target MakeTable
[ 12%] Building CXX object MathFunctions/CMakeFiles/MakeTable.dir/MakeTable.cxx.o
[ 25%] Linking CXX executable MakeTable
[ 25%] Built target MakeTable
[ 37%] Generating Table.h
Generate Table.h file
Scanning dependencies of target MathFunctions
[ 50%] Building CXX object MathFunctions/CMakeFiles/MathFunctions.dir/MakeTable.cxx.o
[ 62%] Building CXX object MathFunctions/CMakeFiles/MathFunctions.dir/mysqrt.cxx.o
[ 75%] Linking CXX static library libMathFunctions.a
[ 75%] Built target MathFunctions
Scanning dependencies of target Tutorial
[ 87%] Building CXX object CMakeFiles/Tutorial.dir/tutorial.cxx.o
[100%] Linking CXX executable Tutorial
[100%] Built target Tutorial
```

然后分别运行`cpack`命令：

```
$ cpack --config CPackConfig.cmake
CPack: Create package using STGZ
CPack: Install projects
CPack: - Run preinstall target for: Tutorial
CPack: - Install project: Tutorial
CPack: Create package
CPack: - package: /work/study/Studied_Module/Module/cmake/13_basic_installer/build/Tutorial-1.0.1-Linux.sh generated.
CPack: Create package using TGZ
CPack: Install projects
CPack: - Run preinstall target for: Tutorial
CPack: - Install project: Tutorial
CPack: Create package
CPack: - package: /work/study/Studied_Module/Module/cmake/13_basic_installer/build/Tutorial-1.0.1-Linux.tar.gz generated.
CPack: Create package using TZ
CPack: Install projects
CPack: - Run preinstall target for: Tutorial
CPack: - Install project: Tutorial
CPack: Create package
CPack: - package: /work/study/Studied_Module/Module/cmake/13_basic_installer/build/Tutorial-1.0.1-Linux.tar.Z generated.
$ cpack --config CPackSourceConfig.cmake
CPack: Create package using TBZ2
CPack: Install projects
CPack: - Install directory: /work/study/Studied_Module/Module/cmake/13_basic_installer
CPack: Create package
CPack: - package: /work/study/Studied_Module/Module/cmake/13_basic_installer/build/Tutorial-1.0.1-Source.tar.bz2 generated.
CPack: Create package using TGZ
CPack: Install projects
CPack: - Install directory: /work/study/Studied_Module/Module/cmake/13_basic_installer
CPack: Create package
CPack: - package: /work/study/Studied_Module/Module/cmake/13_basic_installer/build/Tutorial-1.0.1-Source.tar.gz generated.
CPack: Create package using TXZ
CPack: Install projects
CPack: - Install directory: /work/study/Studied_Module/Module/cmake/13_basic_installer
CPack: Create package
CPack: - package: /work/study/Studied_Module/Module/cmake/13_basic_installer/build/Tutorial-1.0.1-Source.tar.xz generated.
CPack: Create package using TZ
CPack: Install projects
CPack: - Install directory: /work/study/Studied_Module/Module/cmake/13_basic_installer
CPack: Create package
CPack: - package: /work/study/Studied_Module/Module/cmake/13_basic_installer/build/Tutorial-1.0.1-Source.tar.Z generated.
```

最后，使用ll查看build目录下生成的各种安装包：

```
$ ll
total 6512
-rw-rw-r--.  1 study study   20754 Mar 12 16:22 CMakeCache.txt
drwxrwxr-x. 34 study study    4096 Mar 12 16:23 CMakeFiles
-rw-rw-r--.  1 study study    2846 Mar 12 16:22 cmake_install.cmake
-rw-r--r--.  1 study study    3696 Mar 12 16:22 CPackConfig.cmake
drwxrwxr-x.  4 study study      39 Mar 12 16:23 _CPack_Packages
-rw-r--r--.  1 study study    4171 Mar 12 16:22 CPackSourceConfig.cmake
-rw-rw-r--.  1 study study    1158 Mar 12 16:22 CTestTestfile.cmake
-rw-r--r--.  1 study study    2828 Mar 12 16:22 DartConfiguration.tcl
-rw-rw-r--.  1 study study     521 Mar 12 16:23 install_manifest.txt
-rw-rw-r--.  1 study study   22717 Mar 12 16:22 Makefile
drwxrwxr-x.  3 study study     125 Mar 12 16:22 MathFunctions
drwxrwxr-x.  3 study study      23 Mar 12 16:22 Testing
-rwxrwxr-x.  1 study study    8776 Mar 12 16:22 Tutorial
-rwxrwxrwx.  1 study study   12831 Mar 12 16:23 Tutorial-1.0.1-Linux.sh
-rw-rw-r--.  1 study study    4454 Mar 12 16:23 Tutorial-1.0.1-Linux.tar.gz
-rw-rw-r--.  1 study study    6151 Mar 12 16:23 Tutorial-1.0.1-Linux.tar.Z
-rw-rw-r--.  1 study study   69288 Mar 12 16:23 Tutorial-1.0.1-Source.tar.bz2
-rw-rw-r--.  1 study study  339971 Mar 12 16:23 Tutorial-1.0.1-Source.tar.gz
-rw-rw-r--.  1 study study  475996 Mar 12 16:23 Tutorial-1.0.1-Source.tar.xz
-rw-rw-r--.  1 study study 5642777 Mar 12 16:23 Tutorial-1.0.1-Source.tar.Z
-rw-r--r--.  1 study study     491 Mar 12 16:22 TutorialConfig.h
```

详细的介绍，可搜索cpack生成安装包。

# 十四、添加对`Dashboard`的支持

添加我们的测试结果对`Dashboard`的支持非常的容易。前面我们已经添加了很多测试用例，我们当前只需要运行这些测试，并将测试结果提交到`Dashboard`即可。

要添加对`Dashboard`的支持，仅仅只需要添加CTest模块，并设置项目的名字即可。因此在顶层`的CMakeLists.txt`添加如下两行：

```
# enable dashboard scripting
include (CTest)
set (CTEST_PROJECT_NAME "Tutorial")
```

然后，配置（cmake）编译（make）过程与之前一样，编译完成后，切换到可执行程序目录，然后运行如下命令的一条：

```
  ctest -D Continuous
  ctest -D Continuous(Start|Update|Configure|Build)
  ctest -D Continuous(Test|Coverage|MemCheck|Submit)
  ctest -D Experimental
  ctest -D Experimental(Start|Update|Configure|Build)
  ctest -D Experimental(Test|Coverage|MemCheck|Submit)
  ctest -D Nightly
  ctest -D Nightly(Start|Update|Configure|Build)
  ctest -D Nightly(Test|Coverage|MemCheck|Submit)
  ctest -D NightlyMemoryCheck
```

最后，测试的结果将会被上传到[`Kitware's public dashboard`](https://open.cdash.org/index.php?project=PublicDashboard)。

# 十五、添加`make uninstall`的支持

默认情况下，CMake不提供`make uninstall`目标，因此运行`make uninstall`会报找不到目标的错误。我们不希望`make uninstall`命令删除系统有用的文件。

如果你想添加`uninstall`到你的工程（也没有人阻止你这么做），你需要添加一个`install_manifest.txt`文件用来记录所有安装的文件。下面就是一个通用的方法自动生成你需要添加一个`install_manifest.txt`文件的方法。

首先在项目顶层目录下面创建`cmake_uninstall.cmake.in`文件，内容如下：

```
if(NOT EXISTS "@CMAKE_BINARY_DIR@/install_manifest.txt")
  message(FATAL_ERROR "Cannot find install manifest: @CMAKE_BINARY_DIR@/install_manifest.txt")
endif(NOT EXISTS "@CMAKE_BINARY_DIR@/install_manifest.txt")

file(READ "@CMAKE_BINARY_DIR@/install_manifest.txt" files)
string(REGEX REPLACE "\n" ";" files "${files}")
foreach(file ${files})
  message(STATUS "Uninstalling $ENV{DESTDIR}${file}")
  if(IS_SYMLINK "$ENV{DESTDIR}${file}" OR EXISTS "$ENV{DESTDIR}${file}")
    exec_program(
      "@CMAKE_COMMAND@" ARGS "-E remove \"$ENV{DESTDIR}${file}\""
      OUTPUT_VARIABLE rm_out
      RETURN_VALUE rm_retval
      )
    if(NOT "${rm_retval}" STREQUAL 0)
      message(FATAL_ERROR "Problem when removing $ENV{DESTDIR}${file}")
    endif(NOT "${rm_retval}" STREQUAL 0)
  else(IS_SYMLINK "$ENV{DESTDIR}${file}" OR EXISTS "$ENV{DESTDIR}${file}")
    message(STATUS "File $ENV{DESTDIR}${file} does not exist.")
  endif(IS_SYMLINK "$ENV{DESTDIR}${file}" OR EXISTS "$ENV{DESTDIR}${file}")
endforeach(file)
```

接下来在项目顶层目录的`CMakeLists.txt`中添加如下支持：

```
# uninstall target
if(NOT TARGET uninstall)
    configure_file(
        "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
        "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
        IMMEDIATE @ONLY)

    add_custom_target(uninstall
        COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
endif()
```

现在，当你编译、安装库后，就可以通过`make uninstall`命令卸载对应的库了。

如基于第七章的实例，添加上面支持后，当执行`make install`后，会自动在build目录下生成`install_manifest.txt`文件：

```
/work/study/Studied_Module/Module/cmake/07_basic_install/linux/bin/Tutorial
/work/study/Studied_Module/Module/cmake/07_basic_install/linux/include/TutorialConfig.h
/work/study/Studied_Module/Module/cmake/07_basic_install/linux/lib/libMathFunctions.so.0.1
/work/study/Studied_Module/Module/cmake/07_basic_install/linux/lib/libMathFunctions.so.1
/work/study/Studied_Module/Module/cmake/07_basic_install/linux/lib/libMathFunctions.so
/work/study/Studied_Module/Module/cmake/07_basic_install/linux/include/mysqrt.h
```
