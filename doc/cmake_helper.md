# cmake常用方法 #

## 一、常用变量

- `PROJECT_NAME`：指向当前项目的项目名称；
- `CMAKE_PROJECT_NAME`：指向顶层项目的项目名称；
- `CMAKE_BINARY_DIR`：指向最外层项目的顶层构建路径；
- `CMAKE_SOURCE_DIR`：指向最外层项目入口`CMakeLists.txt`文件的目录；
- `PROJECT_BINARY_DIR`：指向当前项目的顶层构建路径；
- `PROJECT_SOURCE_DIR`：指向当前项目入口`CMakeLists.txt`文件的目录；
- `CMAKE_CURRENT_SOURCE_DIR`：指向当前`CMakeLists.txt`文件的目录；

- `LINK_LIBRARIES`：指定要链接的库文件：`LINK_LIBRARIES(pthread rt)`；
- `LINK_DIRECTORIES`：指定要链接库文件的路径：`link_directories($ENV{GFLAGS_LIB_DIR})`；
- `INCLUDE_DIRECTORIES`：指定依赖头文件的路径：`include_directories($ENV{GFLAGS_INC_DIR})`；

- `CMAKE_C_FLAGS`：指定C语言编译参数：`-DCMAKE_C_FLAGS="-Wall -Werror"`；
- `CMAKE_CXX_FLAGS`：指定`C++`编译参数：`-DCMAKE_CXX_FLAGS="-Wall -Werror -std=c++17"`；
- `CMAKE_C_COMPILER`：指定C语言编译工具：`-DCMAKE_C_COMPILER=/usr/bin/cc`；
- `CMAKE_CXX_COMPILER`：指定`C++`编译工具：`-DCMAKE_CXX_COMPILER=/usr/bin/c++`；
- `CMAKE_BUILD_TYPE`：指定构建类型：`-DCMAKE_BUILD_TYPE=Release | Debug`；
- `CMAKE_TOOLCHAIN_FILE`：指定交叉编译工具：`-DCMAKE_TOOLCHAIN_FILE=$toolchain`；
- `CMAKE_INSTALL_PREFIX`：指定构建安装的目录：`cmake .. -DCMAKE_INSTALL_PREFIX=$(pwd)/../linux`；


